
find_path(BULLET3_INCLUDE_DIR 
    NAMES btBulletDynamicsCommon.h
    PATH_SUFFIXES include/bullet include
)

find_library(BULLET3_COLLISION_LIBRARY
    NAMES   BulletCollision
    PATH_SUFFIXES lib64 lib
    )

find_library(BULLET3_DYNAMICS_LIBRARY
    NAMES   BulletDynamics
    PATH_SUFFIXES lib64 lib
    )

find_library(BULLET3_SOFTBODY_LIBRARY
    NAMES   BulletSoftBody
    PATH_SUFFIXES lib64 lib
    )

find_library(BULLET3_LINEARMATH_LIBRARY
    NAMES   LinearMath
    PATH_SUFFIXES lib64 lib
    )

mark_as_advanced(BULLET3_INCLUDE_DIR)

if(BULLET3_COLLISION_LIBRARY AND BULLET3_DYNAMICS_LIBRARY AND BULLET3_SOFTBODY_LIBRARY AND BULLET3_LINEARMATH_LIBRARY)
    set(BULLET3_LIBRARIES
        ${BULLET3_DYNAMICS_LIBRARY}
        ${BULLET3_COLLISION_LIBRARY}
        ${BULLET3_LINEARMATH_LIBRARY}
        ${BULLET3_SOFTBODY_LIBRARY}
    )
endif()

set(BULLET3_FOUND FALSE)

if(BULLET3_INCLUDE_DIR AND BULLET3_LIBRARIES)
    set(BULLET3_FOUND TRUE)
endif()

if(BULLET3_FOUND)
    if(NOT BULLET3_FIND_QUIETLY)
        message(STATUS "Found BULLET3")
    endif()
else()
    if(BULLET3_FIND_REQUIRED)
        message(FATAL_ERROR "Could not find BULLET3")
    endif()
endif()
