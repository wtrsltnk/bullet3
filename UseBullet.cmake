#                                               -*- cmake -*-
#
#  UseBullet.cmake
#


add_definitions     ( ${BULLET3_DEFINITIONS} )
include_directories ( ${BULLET3_INCLUDE_DIRS} )
link_directories    ( ${BULLET3_LIBRARY_DIRS} )

